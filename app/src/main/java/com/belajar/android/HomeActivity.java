package com.belajar.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

//        final SharedPreferences pref = UserPreference.getPref(this);

        UserData userData = UserPreference.getUserData(this);
//        String username = pref.getString(UserPreference.USERNAME, "");
//        String password = pref.getString(UserPreference.PASSWORD, "");

        TextView textUsername = findViewById(R.id.textUsername);
        TextView textPassword = findViewById(R.id.textPassword);

        textUsername.setText(userData.username);
        textPassword.setText(userData.password);

        findViewById(R.id.btnLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeUserCredential();
                goToLogin();
            }
        });
    }

    private void removeUserCredential() {
        UserPreference.destroyPref(this);
    }

    private void goToLogin() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
