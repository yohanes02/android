package com.belajar.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkUserIsLoggedIn();

        setContentView(R.layout.activity_preference);

        final EditText inputUsername = findViewById(R.id.username);
        final EditText inputPassword = findViewById(R.id.password);
        Button btnLogin = findViewById(R.id.login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = inputUsername.getText().toString();
                String password = inputPassword.getText().toString();
                Log.d("MYTAG", username + " : " + password);

                userSaveCredential(username, password);
                goToHome();
            }
        });
    }

    private void checkUserIsLoggedIn() {
        if(UserPreference.checkIsUserLoggedIn(this)) {
            goToHome();
        }
    }

    private void goToHome() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void userSaveCredential(String username, String password) {
        UserPreference.saveUserCredential(this, username, password);
    }
}
