package com.belajar.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    // ctrl + o -> override method
    // ctrl + i -> implement method
    // ctrl + alt + c -> create constant
    // alt + j -> select same word / text
    // ctrl + right click -> go to definition
    // shift + f6
    // ctrl + alt + v

    public static final String MY_TAG = "MY_TAG";
    public static final String PARAM_TEXT2 = "text2";
    public static final String PARAM_TEXT1 = "text1";
    public static final String PARAM_ANGKA1 = "angka1";
    public static final String PARAM_PECAHAN1 = "angka1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button tombol1 = findViewById(R.id.tombol1);
        tombol1.setOnClickListener(new MyClickListener());
    }

    class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Log.d(MY_TAG, "button clicked");

            // Change Activity
            Intent intent = new Intent(MainActivity.this, BelajarActivity.class);
            intent.putExtra(PARAM_TEXT1, "Hallo Semua");
            intent.putExtra(PARAM_TEXT2, "Saya sedang belajar android");
            intent.putExtra(PARAM_ANGKA1, 1);
            intent.putExtra(PARAM_PECAHAN1, 1.6);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(MY_TAG, "on start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MY_TAG, "on resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(MY_TAG, "on pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(MY_TAG, "on stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(MY_TAG, "on destroy");
    }
}
