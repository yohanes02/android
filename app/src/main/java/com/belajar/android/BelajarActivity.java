package com.belajar.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class BelajarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_belajar);

        TextView tv1 = findViewById(R.id.textView);
        TextView tv2 = findViewById(R.id.textView2);

        Intent intent = getIntent();
        String text1 = intent.getStringExtra(MainActivity.PARAM_TEXT1);
        String text2 = intent.getStringExtra(MainActivity.PARAM_TEXT2);

        tv1.setText(text1);
        tv2.setText(text2);
    }
}
