package com.belajar.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserPreference {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String USER_SESSION = "user_session";

    public static SharedPreferences getPref(Context context) {
//        return PreferenceManager
//                .getDefaultSharedPreferences(context);
        return context.getSharedPreferences(USER_SESSION, Context.MODE_PRIVATE);
    }

    public static void destroyPref(Context context) {
        SharedPreferences.Editor edit = getPref(context).edit();
        edit.clear();
        edit.apply();
    }

    public static  void saveUserCredential(Context context, String username, String password) {
        SharedPreferences pref = UserPreference.getPref(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(UserPreference.USERNAME, username);
        editor.putString(UserPreference.PASSWORD, password);
        editor.apply();
    }

    public static boolean checkIsUserLoggedIn(Context context) {
        SharedPreferences pref = getPref(context);
        String username = pref.getString(UserPreference.USERNAME, null);
        String password = pref.getString(UserPreference.PASSWORD, null);
        return username != null && password != null;
    }

    public static UserData getUserData(Context context) {
        SharedPreferences pref = getPref(context);
        String username = pref.getString(UserPreference.USERNAME, null);
        String password = pref.getString(UserPreference.PASSWORD, null);
        return new UserData(username, password);
    }
}
